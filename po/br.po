# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-06-16 13:41+0100\n"
"PO-Revision-Date: 2013-04-08 07:29-0400\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Breton (http://www.transifex.com/projects/p/libvirt-glib/"
"language/br/)\n"
"Language: br\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Zanata 3.6.2\n"

#: ../libvirt-gconfig/libvirt-gconfig-helpers.c:141
msgid "No XML document to parse"
msgstr ""

#: ../libvirt-gconfig/libvirt-gconfig-helpers.c:149
msgid "Unable to parse configuration"
msgstr ""

#: ../libvirt-gconfig/libvirt-gconfig-helpers.c:157
#, c-format
msgid "XML data has no '%s' node"
msgstr ""

#: ../libvirt-gconfig/libvirt-gconfig-object.c:212
msgid "No XML document associated with this config object"
msgstr ""

#: ../libvirt-gconfig/libvirt-gconfig-object.c:221
#, c-format
msgid "Unable to create RNG parser for %s"
msgstr ""

#: ../libvirt-gconfig/libvirt-gconfig-object.c:231
#, c-format
msgid "Unable to parse RNG %s"
msgstr ""

#: ../libvirt-gconfig/libvirt-gconfig-object.c:243
#, c-format
msgid "Unable to create RNG validation context %s"
msgstr ""

#: ../libvirt-gconfig/libvirt-gconfig-object.c:253
msgid "Unable to validate doc"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:434
#, c-format
msgid "Connection %s is already open"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:449
#, c-format
msgid "Unable to open %s"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:460
msgid "Unable to get connection URI"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:700
#, c-format
msgid "Unable to count %s"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:712
#, c-format
msgid "Unable to list %s %d"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:755
#: ../libvirt-gobject/libvirt-gobject-connection.c:892
msgid "Connection is not open"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:770
msgid "Unable to count domains"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:781
msgid "Unable to list domains"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:1155
#: ../libvirt-gobject/libvirt-gobject-connection.c:1194
msgid "Connection is not opened"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:1162
msgid "Unable to get hypervisor name"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:1200
msgid "Unable to get hypervisor version"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:1515
#: ../libvirt-gobject/libvirt-gobject-connection.c:1570
msgid "Failed to create domain"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:1622
msgid "Failed to create storage pool"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:1665
msgid "Unable to get node info"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:1705
msgid "Unable to get capabilities"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-connection.c:1833
msgid "Unable to restore domain"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-stream.c:339
msgid "virStreamRecv call would block"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-stream.c:342
#: ../libvirt-gobject/libvirt-gobject-stream.c:458
#, c-format
msgid "Got virStreamRecv error in %s"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-stream.c:407
msgid "Unable to perform RecvAll"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-stream.c:455
msgid "virStreamSend call would block"
msgstr ""

#: ../libvirt-gobject/libvirt-gobject-stream.c:523
msgid "Unable to perform SendAll"
msgstr ""
